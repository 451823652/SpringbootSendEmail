package com.youthcool.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {
	@ResponseBody
	@RequestMapping("/")
	public ModelAndView index(ModelMap map) {
		return new ModelAndView("/index");
	}
}
