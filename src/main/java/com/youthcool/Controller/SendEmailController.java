package com.youthcool.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.youthcool.Service.SendEmailService;
@RestController
@RequestMapping("/SendEmail")
public class SendEmailController
{
	@Autowired
	SendEmailService sendemailservice;

	//@CrossOrigin("http://127.0.0.1:8020")
	@RequestMapping(value = "/Send", method = RequestMethod.POST)
	public boolean Send(@RequestParam("from") String from, @RequestParam("to") String to,
			@RequestParam("Subject") String Subject, @RequestParam("Text") String Text)
	{
		boolean result = sendemailservice.sendSimpleMail(from, to, Subject, Text);
		return result;
	}
}
