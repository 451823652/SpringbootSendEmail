package com.youthcool.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
@Service
public class SendEmailService {
	@Autowired
	private JavaMailSender JMSender;
	//简单邮件

	public Boolean sendSimpleMail(String from, String to, String Subject, String Text)
	{
		boolean result = false;
		try
		{
			SimpleMailMessage SMessage = new SimpleMailMessage();
			SMessage.setFrom(from);
			SMessage.setTo(to);
			SMessage.setSubject(Subject);
			SMessage.setText(Text);
			JMSender.send(SMessage);
			result = true;
		} catch (Exception e)
		{
			
		}
		return result;
	}
}
