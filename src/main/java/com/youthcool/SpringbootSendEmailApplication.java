package com.youthcool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages= {"com.youthcool"})
public class SpringbootSendEmailApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringbootSendEmailApplication.class, args);
	}
}
